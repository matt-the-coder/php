<html>
    <head>
        <title>Add Person</title>
        <link rel="stylesheet" href="styles.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                $('input[name=username]').keyup(function() {
                    var username = $('input[name=username').val();
                    $('#isTaken').load("istaken.php?username=" + username);
                });
            });
        </script>
    </head>
<body>
    <div id="centerContent">
        <?php
        require_once 'db.php';
function getForm($nameVal = "", $gpaVal = "", $isGraduateVal = false, $genderVal="male") {    
$form = <<< ENDMARKER
<form method="post">
    Name: <input type="text" name="name" value="$nameVal">
        <span class="errorMessage" id="isTaken"></span><br>
    GPA: <input type="text" name="gpa" value="$gpaVal"><br>
    Is Graduate; <input type="checkbox" name="isGraduate"><br>
    Gender:<br><input type="radio" id="male" name="gender" value="male"
         checked><label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female"><label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other"><label for="other">Other</label><br>
    <input type="submit" value="Register">
</form>
ENDMARKER;
return $form;
}//end form

if (isset($_POST['name'])) {
    $name = $_POST['name'];
    $gpa = $_POST['gpa'];
    $isGraduate = isset($_POST('isGraduate'));
    $gender = "male";
    $errorList = array();
    
    if (preg_match('/^.{1,50}$/', $name) != 1) {
        array_push($errorList, "Name must be 1-50 characters long.");
        $name = "";
    }
    
    if (preg_match("/^[0]|[0-3].(d?d?)|[4].[3]$/", $gpa)) {
        array_push($errorList, "GPA must be between 0 and 4.3.");
        $gpa = "";
    }


if ($errorList) { // array not empty -> errors present
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($name, $gpa, $isGraduate, $gender);
    } else {
        // STATE 3: Successful submission
        echo "<p>Registration successful</p>";
        // FIXME: Security hole here!!!! SQL INJECTION
        $result = mysqli_query($link, sprintf("INSERT INTO people VALUES (NULL, '%s', '%s', '%s')",
            mysqli_real_escape_string($link, $name),
            mysqli_real_escape_string($link, $gpa),
            mysqli_real_escape_string($link, $isGraduate ? 'true' : 'false'),
            mysqli_real_escape_string($link, $gender)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
    }
} else { 
    // STATE 1: First show
    echo getForm();

}










?>
    </div>
</body>
</html>