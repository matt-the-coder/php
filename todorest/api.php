<?php

session_cache_limiter(false);
session_start();

require_once 'vendor/autoload.php';
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('errors.log', Logger::ERROR));

// database 
if ($_SERVER['SERVER_NAME'] == 'localhost') {
DB::$user = 'todorest17';
DB::$password = 'pMX7i4mSmGhVUZy0';
DB::$dbName = 'todorest17';
DB::$encoding = 'utf8';
DB::$port = 3333;
DB::$error_handler = 'database_error_handler';
DB::$nonsql_error_handler = 'database_error_handler';
}

function database_error_handler($params) {
    global $app, $log;
    $log->error("SQL Error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL Query: " . $params['query']);
    }
    echo json_encode("500 - internal error");
    http_response_code(500);
    die(); // don't want to keep going if a query broke
}

$app = new \Slim\Slim();

$app->response()->header('content-type', 'application/json');
// Slim creation and setup

\Slim\Route::setDefaultConditions(array(
    'id' => '[1-9][0-9]*'
));

function getUserIpAddr() {
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

//fetch multiple items
$app -> get('/todos', function() use ($app, $log) {
    $list = DB::query('SELECT * FROM todos');
    echo json_encode($list, JSON_PRETTY_PRINT);
    
});

//fetch single items
$app -> get('/todos/:id', function($id) use ($app, $log) {
    $list = DB::queryFirstRow('SELECT * FROM todos WHERE id=%i', $id);
    if ($item) {
    echo json_encode($list, JSON_PRETTY_PRINT);
    } else {
        $app->response()->setStatus(404);
        echo json_encode("404 - Not found");
    }
    
});

$app->post('/todos', function() use ($app, $log) {
    $json = $app->request()->getBody();
    $todo = json_decode($json, true);
    //todo verify data before inerting
    DB::insert('todos', $todo);
    echo DB::insertID();
});

$app->put('.todo/:id', function($id) use ($app, $log) {
    $json = $app->request()->getBody();
    $todo = json_decode($json, true);
    //todo verify data before inerting
    unset($todo['id']); //prevent changing record id
    DB::update('todos', $todo, 'id=%i', $id);
    echo json_encode(true);
});

$app->delete('.todo/:id', function($id) use ($app, $log) {
    DB::delete('todos', $todo, 'id=%i', $id);
    echo json_encode(true);
});

$app->run();

