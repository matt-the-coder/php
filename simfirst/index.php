<?php

require_once 'vendor/autoload.php';

DB::$user = 'slimfirst17';
DB::$password = 'tnhXSAX1Y1DDboN0';
DB::$dbName = 'slimfirst17';
DB::$encoding = 'utf8';
DB::$port = 3333;

//tnhXSAX1Y1DDboN0

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

$app->get('/hello/:name', function ($name) {
    echo "Hello, " . $name;
});

$app->get('/hello/:name/:age', function ($name,$age) use ($app){
    DB::insert('people', array(
  'name' => $name,
  'age' => $age
));
    $app->render('newTwigTemplate.html.twig', array('name' => $name, 'age' => $age));
    //echo "<p>Hello, $name, you are $age years old</p>";
});

$app->get('/people/add', function() use ($app){
    
    $app->render('people_add.html.twig');
});

$app->post('/people/add', function() use ($app) {
    
    $name = $app->request()->post('name');
    $age = $app->request()->post('age');
    
    $errorlist = array();
    
    if (strlen($name) < 2 || strlen($name) > 100) {
        array_push($errorlist, "Name must be 2-100 characters");
        $name="";
    }
    
    if ($age == '' || $age< 0 ||$age > 150) {
        array_push($errorlist, "Age must be 1-150");
        $age="";
    }
    if ($errorlist) { //state 2: failed submissions
        $app->render('people_add.html.twig', array('errorlist' => $errorlist, 'v'=> array('name'=>$name, 'age'=>$age)));
    }
    else {
        //Stage 3 succesful  submission
        DB::insert('people',array('name' => $name, 'age' => $age));
        $app-> render('people_add_success.html.twig');
    }
});

$app->run();