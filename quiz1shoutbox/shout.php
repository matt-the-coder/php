<!DOCTYPE html>
<html>
    <head>
        <title>Shout</title>
        <link rel="stylesheet" href="styles.css">
    </head>
<body>
    <div id="centerContent">
        <?php
        require_once 'db.php';
function getForm($nameVal = "", $messageVal = "") {    
$form = <<< ENDMARKER
<form method="post">
    Name: <input type="text" name="name" value="$nameVal">
    Message: <input type="text" name="message" value="$messageVal"><br>
    <input type="submit" value="Shout">
</form>
ENDMARKER;
return $form;
}//end form

if (isset($_POST['name'])) {
    $name = $_POST['name'];
    $message = $_POST['message'];
    $errorList = array();
    
    if (preg_match('/^[a-zA-Z0-9]+[a-zA-Z0-9]{2,20}$/', $name) != 1) {
        array_push($errorList, "Name must be 2-20 characters long.");
        $name = "";
    }
    
    if (preg_match('/^.{1,100}$/', $message) != 1) {
        array_push($errorList, "Name must be 1-100 characters long.");
        $message = "";
    }


if ($errorList) {
        // STATE 2: Failed submission
        echo "<p>There were problems with your submission:</p>\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li class=\"errorMessage\">$error</li>\n";
        }
        echo "</ul>\n";
        echo getForm($name, $message);
        
    } else {
        // STATE 3: Successful submission
        echo "<p>Shouted successful</p>";
        
        $result = mysqli_query($link, sprintf("INSERT INTO shouts VALUES (NULL, '%s', '%s', NULL)",
            mysqli_real_escape_string($link, $name),
            mysqli_real_escape_string($link, $message)));
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
    }
} else { 
    // STATE 1: First show
    echo getForm();
   
    $result = mysqli_query($link, "SELECT * FROM shouts ORDER BY tsPosted DESC LIMIT 10");
        if (!$result) {
            echo "SQL Query failed: " . mysqli_error($link);
            exit;
        }
        echo "<ul>\n";
            while ($shoutList = mysqli_fetch_assoc($result)) {
            echo "<li>" . "On " . $shoutList['tsPosted'] . $shoutList['name'] . " shouted: " . $shoutList['message'] . "</li>";
        }
        echo "</ul>\n\n";

    
    //session counter
    if(!isset($_SESSION['count'])) {
    $_SESSION['count'] = 0;
    }

    $_SESSION['count']++;

    echo"You have shouted " . $_SESSION['count'] . " times from this browser session.";

    
}










?>
    </div>
</body>
</html>